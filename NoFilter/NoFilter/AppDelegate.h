//
//  AppDelegate.h
//  NoFilter
//
//  Created by fanqingyi on 1/17/15.
//  Copyright (c) 2015 Qingyi Fan. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

